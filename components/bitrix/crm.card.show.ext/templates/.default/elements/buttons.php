<div class="ui divider"></div>
<div class="ui-btn-container ui-btn-container-center">
    <?
    $collect = '';
    foreach ( $additionalButtons as $buttonId => $class )
    {
        $data = explode('|', $class);

        $collect .= '<button onClick="event.preventDefault(); false;" id="'.$buttonId.'" class="'.$globalButtonClasses.' '. $data[0] .'">';

        if ( $data[1] )
            $collect .= '<i style="margin: 0 8px 0 -5px;" class="'.trim($data[1]).'"></i>';

        $collect .=  trim(ucwords($buttonId));
        $collect .=  '</button>';
    }
    echo $collect;
    ?>
</div>