<div type="folder" class="<?=pathinfo(__FILE__)['filename']?>" style="display:none">

    <h5 class="ui header">
        <i class="<?=explode('|',$additionalButtons['junk'])[1]?>"></i>
        <div class="content">
            Please Select a Reason Why:
        </div>
    </h5>

    <div class="ui fluid search selection dropdown drpdwn" id="<?=pathinfo(__FILE__)['filename']?>">
        <input type="hidden" name="STATUS_ID[JUNK]" value="<?=$arResult['ENTITY_DATA']['STATUS_ID'];?>">
        <div class="default text">Select a Reason</div>
        <i class="dropdown icon"></i>
        <div class="menu" style="max-height: 21rem;">
            <? foreach( $arResult['JUNK'] as $id => $name ):?>
                <div class="item <?if($arResult['ENTITY_DATA']['STATUS_ID'] == $id) echo 'selected';?>" data-value="<?=$id?>"><?=$name;?></div>
            <?endforeach;?>
        </div>
    </div>

</div>