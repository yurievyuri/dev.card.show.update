<div type="folder" class="<?=pathinfo(__FILE__)['filename']?>" style="display:none">

    <h5 class="ui header">
        <i class="<?=explode('|',$additionalButtons['existing'])[1]?>"></i>
        <div class="content">
            Entity Search:
        </div>
    </h5>

    <div class="field">
        <div class="ui fluid category search">
            <div class="ui icon input">
                <input class="prompt" type="text" name="SEARCH" placeholder="Search contacts by name, phone, email, id...">
                <i class="search icon"></i>
            </div>
            <div class="results"></div>
        </div>
    </div>

    <div class="field selectedcard">

        <input type="hidden" name="PIN_VALUE" data-type-id="" value="">
        <input type="hidden" name="PIN_TYPE" data-type-id="" value="">

        <div class="ui message hidden" style="/*padding:1em 1.5em 0.4em 1.5em;*/"></div>

        <div class="ui message hidden basic">
            <div class="content" style="/*padding:1em 1.5em 0.1em 1.5em;*/">
                <div class="header"></div>
                <div class="meta right floated mini ui"></div>
                <div class="description" style="margin-top: 0.8em;"></div>
            </div>
            <div class="extra content">
                <div class="right floated" style="float: right;"></div>
            </div>
        </div>

    </div>

</div>