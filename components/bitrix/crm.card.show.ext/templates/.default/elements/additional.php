<?php
// Additional Information
foreach ( $additionalButtons as $buttonId => $value )
{
    $path = $_SERVER['DOCUMENT_ROOT'] . $templateFolder . '/elements/additionals/' . $buttonId . 'Content.php';
    if (file_exists($path))
        require_once $path;
    else
    {
        $fp = fopen($path, 'a');
        fwrite($fp, '<?php');
        fclose($fp);
    }
}