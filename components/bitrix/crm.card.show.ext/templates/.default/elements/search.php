<div class="" style="margin-top: 15px;">
    <?$propName = 'SEARCH';?>
    <label><small><?//=$component->prefetchCorrectFieldName($propName, $arResult);?></small></label>
    <?
    $setup = [
        "ID" => 'test',
        "LAZYLOAD" => 'Y',
        "INPUT_NAME" => $propName . '[]',
        "USE_SYMBOLIC_ID" => "N",
        "BUTTON_SELECT_CAPTION" => 'search global', // \Bitrix\Main\Localization\Loc::getMessage("ECLF_DESTINATION_ADD_USERS"),
        "BUTTON_SELECT_CAPTION_MORE" => 'search', \Bitrix\Main\Localization\Loc::getMessage("ECLF_DESTINATION_ADD_MORE"),
        "DUBLICATE" =>  "N",
        "MULTIPLE"  =>  "Y",
        'API_VERSION' => '3',
        "SELECTOR_OPTIONS" => array(

            'lazyLoad' => 'Y',
            'context' => $propName,
            'contextCode' => 'CRM',
            'enableUsers' => 'N',
            'enableSonetgroups' => 'N',
            'enableDepartments' => 'N',
            'allowAddSocNetGroup' => 'N',
            'departmentSelectDisable' => 'Y',
            'showVacations' => 'Y',
            'dublicate' => 'N',
            //'userSearchArea' => 'I', // E
            'enableAll' => 'N',
            'departmentFlatEnable' => 'Y',
            'useSearch' => 'Y',
            'multiple' => 'Y',
            'enableEmpty' => 'N',

            'enableCrm' => 'Y',
            'returnItemUrl' => 'Y',
            
            'enableCrmLeads' => 'Y',
            'enableCrmContacts' => 'Y',
            'enableCrmCompanies' => 'Y',
            'enableCrmDeals' => 'Y',
            'enableCrmProducts' => 'Y',
            //'enableCrmQuotes' => 'Y',
            //'enableCrmOrders' => 'Y',

            'addTabCrmLeads' => 'Y',
            'addTabCrmContacts' => 'Y',
            'addTabCrmCompanies' => 'Y',
            'addTabCrmDeals' => 'Y',
            'addTabCrmProducts' => 'Y',
            //'addTabCrmQuotes' => 'Y',
            //'addTabCrmOrders' => 'Y',
        )
    ];
    $APPLICATION->IncludeComponent(
        "bitrix:main.user.selector",
        "",
        $setup
    );
    ?>
</div>


<!--div class="ui loading fluid multiple search selection dropdown">
    <input type="hidden" name="country" value="kp">
    <i class="dropdown icon"></i>
    <input class="search">
    <div class="default text">Search...</div>
    <div class="menu">
        <div class="item">Choice 1</div>
        <div class="item">Choice 2</div>
        <div class="item">Choice 3</div>
    </div>
</div-->