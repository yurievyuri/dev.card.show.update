// init
function extraStart()
{
    'use strict';

    let cached = {},
        delay = 30000,
        main = document.querySelector('body'),
        entityType = main && main.querySelector('[name="ENTITY_TYPE"]').value,
        scrollField = main && main.querySelector('.scrollField'),
        //minimizeButton = main.querySelector('.im-phone-btn-arrow'),
        //typeCardValue = main.querySelector('[name="SIMPLE"]').value ? 'main' : 'detail',
        action = main.querySelector('[name="ACTION"'),
        //mainOverlay = document.getElementById('popup-window-overlay-im-phone-call-view'),
        //mainDefaultZindex = main.style.zIndex,
        //mainOverlayDefaultZindex = mainOverlay.style.zIndex,
        windowWidth = false,
        windowHeight = false,
        mainWidth = false,
        mainHeight = false,
        extraContentHeight = false,
        buttonsContainerHeight = false,
        //mainContent = document.getElementById('popup-window-content-im-phone-call-view'),

        extraContent = document.getElementById('im-phone-call-view-extra'),

        openingContainer = extraContent && extraContent.querySelector('.opening'),
        additionalContainer = extraContent && extraContent.querySelector('.additional'),

        buttonsContainer = main && main.querySelector('.im-phone-call-buttons-container'),
        //callWrapper = main && main.querySelector('.im-phone-call-wrapper'),
        callTopLevel = main && main.querySelector('.im-phone-call-top-level'),
        //callContainer = main && main.querySelector('.im-phone-call-container'),
        defaultButtonId = 'defaultLead',
        saveButtonId = 'saveLead',
        confirmSaveButtonId = 'confirmSaveLead',
        extraButtonContainer = main && main.querySelector('#extraButtonContainer'),
        //extraButtonId = 'getLeadUpdateForm',
        //extraButtonContainerId = 'extraButtonContainer',
        alertContainerId = 'alertSection',
        //extraButtonContainerHtml = '',

        alertCurentClass = 'ui-alert-success',
        setup = {
            'COMPONENT_PATH' : false,
            'TEMPLATE_FOLDER' : false,
            'DEFAULT_PHONES' : false,
            'ID' : false,
            'TYPE' : false
        },
        //buttons = {},
        form = main.querySelector('form'),

        defaultAdditionalActive = 'opening',
        currentAdditionalActive = defaultAdditionalActive,
        currentButtonActive  = false,
        standartButtonClass = 'ui-btn-light-border',
        standartButtonActiveClass = 'ui-btn-danger-dark',
        standartButtonDisableClass = 'ui-btn-disabled',

        selectedCard = extraContent && extraContent.querySelector('.selectedcard'),
        selectedCardData = {
            id: selectedCard && selectedCard.querySelector('[name="PIN_VALUE"]'),
            type: selectedCard && selectedCard.querySelector('[name="PIN_TYPE"]'),
            title: selectedCard && selectedCard.querySelector('.header'),
            meta: selectedCard && selectedCard.querySelector('.meta'),
            description: selectedCard && selectedCard.querySelector('.description'),
            extra: selectedCard && selectedCard.querySelector('.extra').querySelector('.floated'),
            message: selectedCard && selectedCard.querySelector('.message'),
            status: false
        },

        mutationObserver,
        $categorySearch,

        defaultButton = main.querySelector('#' + defaultButtonId),
        saveButton = main.querySelector('#' + saveButtonId),
        confirmSaveButton = main.querySelector('#' + confirmSaveButtonId),
        alertSection = main.querySelector('#' + alertContainerId)
    ;

    // spike for lead entity
    if ( entityType !== 'LEAD' )
    {
        defaultButton.outerHTML = ' ';
        saveButton.outerHTML = ' ';
        return false;
    }

    defaultButton.addEventListener('click', defaultRestoreForm );
    saveButton.addEventListener( 'click', toggleSaveLead );
    confirmSaveButton.addEventListener( 'click', saveLeadForm );

    alertSection.querySelector('.ui-alert-close-btn').addEventListener('click', hideAlert );

    form.addEventListener('submit', function()
    {
        event.preventDefault();
        event.stopPropagation();
        console.log('submit false');
        return false;
    });

    add2Cache();
    getHiddenProperties();
    addTopButtonsEvents();
    getSemantic();
    addCheckFormFields();
    getWindowSize();
    fixScrollFieldSize();
    fixAlertPosition();
    fixAutoComplete();

    function getHiddenProperties()
    {
        Array.from(form.querySelectorAll('[type="hidden"]')).map((item) =>
        {
            if (!item.value) return;
            if( item.getAttribute('name') === 'DEFAULT_PHONES' && item.value ) {
                try {
                    setup[item.getAttribute('name')] = JSON.parse(Base64Decode(item.value));
                } catch (ex) {
                    console.error( 'outer', ex.message );
                }
            }
            else
                setup[ item.getAttribute('name') ] = item.value ? item.value : false;
        });
    }

    /**
     * addTopButtonsEvents
     */
    function addTopButtonsEvents()
    {
        Array.from(form.getElementsByTagName('BUTTON')).map((item) =>
        {
            if (!item.getAttribute('id')) return;
            if( item && standartButtonDisableClass && item.classList.contains( standartButtonDisableClass )) return false;
            //item.addEventListener('click', eval(item.getAttribute('id')) );
            item.addEventListener('click', getAdditional );
        });
    }

    /**
     * activation semantic elements
     * @returns {boolean}
     */
    function getSemantic()
    {
        if( !window.jQuery )
        {
            console.error('Semantic UI uses a jQuery. The execution is stopped...');
            return false;
        }

        $(".ui.drpdwn, .rmt.ui")
            .dropdown({
                clearable: true,
                direction: 'downward'
            })
            .dropdown('save defaults')
        ;

        $('.button')
            .popup({
                inline: true
            })
        ;

        $('.rmt.ui')
            .dropdown({
                /*onShow: function(){

                    fixDropdownMenu();

                }
                apiSettings: {
                    //url: setup.COMPONENT_PATH + '/ajax.php?remoteusergroups&BITRIX_SESSID=' + BX.bitrix_sessid(),
                    cache: false,
                    method: 'POST',
                    serializeForm: true,
                    data: $(form).serializeArray(),
                    fields : {
                        remoteValues : 'results', // grouping for api results
                        values       : 'values', // grouping for all dropdown values
                        name         : 'name',   // displayed dropdown text
                        value        : 'value'   // actual dropdown value
                    },
                    onError: function(errorMessage, element, xhr)
                    {
                        toggleAlert('danger', errorMessage );
                    },
                    beforeSend: function (settings)
                    {
                        hideAlert();
                        return settings;
                    }
                },*/
                //saveRemoteData: true,
                //filterRemoteData: true,
                //placeholder: 'any',
                clearable: true,
                fullTextSearch : true,
                match: 'text',
                ignoreCase: true,
                cache: false,
                //forceSelection: false,
                //allowReselection: false,
                //allowAdditions: false,
                //allowCategorySelection: true
            }).dropdown('save defaults')
        ;

        $categorySearch = $('.ui.category.search');
        $categorySearch
            .search({
                type          : 'category',
                minCharacters : 1,
                //source : 'content',
                showNoResults: true,
                //fullTextSearch : true,
                //searchOnFocus: false,
                maxResults: 20,
                //duration: 50,
                searchDelay: 200,
                //hideDelay: 50,
                //transition: false,
                //easing: false,
                selector : {
                    prompt       : '.prompt',
                    searchButton : '.search.button',
                    results      : '.results',
                    category     : '.category',
                    result       : '.content'
                },
                className: {
                    active  : 'active',
                    empty   : 'empty',
                    focus   : 'focus',
                    loading : 'loading',
                    pressed : 'down'
                },
                searchFields  : [
                    'title',
                    'description'
                ],
                fields: {
                    categories      : 'results',     // array of categories (category view)
                    categoryName    : 'name',        // name of category (category view)
                    categoryResults : 'results',     // array of results (category view)
                    description     : 'description', // result description

                    extra           : 'extra',
                    id              : 'id',
                    type            : 'type',
                    data            : 'data',

                    image           : 'image',       // result image
                    price           : 'price',       // result price
                    results         : 'results',     // array of results (standard)
                    title           : 'title',       // result title
                    action          : 'action',      // "view more" object name
                    actionText      : 'text',        // "view more" text
                    actionURL       : 'url'          // "view more" url
                },
                cache: false,
                apiSettings   :
                    {
                        url: setup.COMPONENT_PATH + '/ajax.php?search={query}',
                        cache: false,
                        method: 'POST',
                        serializeForm: true,
                        data: $(form).serializeArray(),
                        onError: function (errorMessage, element, xhr)
                        {
                            toggleAlert('danger', errorMessage);
                            cached['selectedSearch'] = false;
                        },
                        onComplete : function(response, element, xhr)
                        {
                            hideSelectedCard();
                            return response;
                        },
                        beforeSend: function(settings)
                        {
                            return settings;
                        }
                    },
                onSearchQuery: function(query)
                {
                    cached['selectedSearch'] = false;
                    cached['query'] = query;
                    hideAlert();
                },
                onResults: function (response)
                {
                    cached['selectedSearch'] = false;
                    startMutations();
                },
                onSelect: function ( result )
                {
                    cached['searchedTitle'] = result.title;
                    cached['selectedSearch'] = result;
                    pinSelectedCard(result);
                },
                onResultsOpen : function()
                {
                    scrollField.style.overflowY = 'hidden';
                    scrollField.style.overflowX = 'hidden';
                    fixSearchResultSize();
                    startMutations();
                },
                onResultsAdd: function(html)
                {
                },
                onResultsClose : function()
                {
                    scrollField.style.overflowY = 'auto';
                    scrollField.style.overflowX = 'hidden';
                    stopMutations();
                },

            })

            .dropdown('save defaults')
        ;

        $('.ui.toggle.checkbox')
            .checkbox();
    }

    function prepareContactHTML( object = {} )
    {
        let item = '<div class="ui aligned divided list" style="font-size: 12px">';

        item += getContactHTML(object);

        item += '</div>';

        return item;
    }

    /**
     * @param object
     * @returns {string}
     */
    function prepareMessageHTML( object )
    {
        if ( !setup.DEFAULT_PHONES ) return '';

        if( !object )
            object = {
                data : {
                    id : setup.ID,
                    SEARCHED : setup.DEFAULT_PHONES
                }
            }
            ;

        let

            item = '<div class="header"> Income Data </div>';

        item += '<div class="ui aligned divided list" style="font-size: 12px">'; //font-size: 0.78571429em;

        item += getContactHTML(object);

        item += '</div>';

        return item;
    }

    function getContactHTML( object = {} )
    {
        if (!object.data || !object.data['SEARCHED']) return '';

        let item = '';

        Object.keys(object.data['SEARCHED']).forEach(function (id)
        {
            let value;

            // fix number version
            if(this[id]['VALUE_EXT'])
                value = this[id]['VALUE_EXT'];
            else
                value = this[id]['VALUE'];

            item += '<div class="item padded">';

            item += '<div class="ui grid">';

            item += '<div class="eleven wide column">';

            item +=     '<div class="content">';
            item +=     '<div class="header" style="font-size: 12px">' + getIcon(this[id]) + value + '</div>';

            //if( this[id].DEFAULT )
            //    item += '<i><small>income user numbers</small></i>';

            item +=     '</div>';

            item += '</div>';
            item += '<div class="five wide column">';

            item += getDropMenu( this[id] ); //;

            item += '</div>';

            item += '</div>';

            item += '</div>';

        }, object.data['SEARCHED']);

        return item;
    }

    function getDropMenu( object = {} )
    {
        let defaultIcon = 'bars black',

            html = '<div class="ui mini inline dropdown">' +

                '<div class="text"><i class="' + defaultIcon + ' icon"></i></div>' +

                '<input type="hidden" name="' +

                currentAdditionalActive.toUpperCase() +

                '[' + object.ENTITY_ID + ']' +

                '[' + object.ELEMENT_ID + ']' +

                //'[' + object.TYPE_ID + ']' + PHONE or EMAIL

                '['+ object.ID +']" value="">'+

                '<div class="menu mini">'
        ;

        html += '<div data-value="" class="item">' +
            '<i class="' + defaultIcon + ' icon"></i>default</div>';

        if( object.DEFAULT && object.TYPE_ID === 'PHONE' )
            html += '<div data-value="ADD" class="item">' +
                '<i class="plus green icon"></i>add to this</div>';

        if( !object.DEFAULT )
            html += '<div data-value="DELETE" class="item">' +
                '<i class="trash icon red"></i>delete</div>';


        html += '</div>'; //menu
        html += '</div>'; // dropdown

        return html;
    }

    function getIcon( type = false )
    {
        let name;

        switch (type['TYPE_ID'])
        {
            case 'PHONE':
                name = 'black phone square';
                break;
            case 'EMAIL':
                name = 'black envelope outline';
                break;
            case 'CHAT' :
                name = 'black comment outline';
                break;
            default:
                name = 'black address card outline';
        }

        if ( type.DEFAULT )
            name = 'red question circle outline';

        return '<i class="icon ' + name + '"></i>';
    }

    function dropMenuActivate( object = {} )
    {
        if ( !object ) return false;

        let dropMenu = object && object.querySelectorAll('.ui.inline');

        $(dropMenu).dropdown({
            selector : {
                dropdown     : '.ui.inline.dropdown',
                icon         : '.cogs.icon',
                menuIcon     : '.cogs.icon',
            },
            inline: true
        });
    }

    function pinSelectedCard( data = {} )
    {
        if( selectedCardData.status === false || data )
        {
            showSelectedCard();
        }
        else
        {
            hideSelectedCard();
        }

        if ( !data && cached.selectedSearch )
            data = cached.selectedSearch;

        fillSelectedCard( data );
        fixButtonsClick();
    }

    function fillSelectedCard( data = {} )
    {
        selectedCardData.id.value = data.id;
        selectedCardData.type.value = data.type;

        selectedCardData.title.innerHTML = data.title || '';
        selectedCardData.description.innerHTML = prepareContactHTML( data ); //data.description || '';

        //selectedCardData.meta.innerHTML = data.extra || '';

        selectedCardData.extra.innerHTML = data.extra; //data.extended || '';

        if ( setup.DEFAULT_PHONES )
        {
            //selectedCardData.message.style.display = 'block';
            selectedCardData.message.innerHTML = prepareMessageHTML();
        }
        //else
            //selectedCardData.message.style.display = 'none';

        dropMenuActivate( selectedCard );
    }

    function showSelectedCard()
    {
        Array.from(selectedCard.querySelectorAll('.message.hidden')).map((item) =>
        {
            item.classList.remove('hidden');
        });

        //selectedCard.style.display = 'block';
        selectedCardData.status = true;
    }

    function hideSelectedCard()
    {
        //selectedCard.style.display = 'none';
        selectedCardData.status = false;

        Array.from(selectedCard.querySelectorAll('.message')).map((item) =>
        {
            item.classList.add('hidden');
        });

        Array.from(selectedCard.querySelectorAll('input')).map((item) =>
        {
            item.value = '';
        });

        if ( $categorySearch)
            $categorySearch.search('hide results');

        fillSelectedCard({title: false , meta: false , description: false , extra: false , extended: false, message: false});
    }

    /**
     *
     * @returns {boolean}
     */
    function checkValidForm()
    {
        /*let junk = main && main.querySelector('[name="STATUS_ID[JUNK]"]').value;

        $(form).form('validate form');
        if ( junk && junk !== 'NEW' )
        {
            hideAlert();
            return true;
        }*/

        let error = 0;

        Array.from(form.querySelectorAll('[required="required"]')).map((item) =>
        {
            if ( !item.value )
            {
                item.parentNode.parentNode.classList.add('error');
                error++;
            }
            else
                item.parentNode.parentNode.classList.remove('error');

        });

        if ( error > 0 )
        {
            toggleAlert( 'warning', 'Fill in the following fields: ' +  collectFieldsErrors());
            return false
        }
        else
        {
            hideAlert();
            return true;
        }
    }

    /***
     *
     * @returns {string}
     */
    function collectFieldsErrors()
    {
        let fieldName = [];
        Array.from(extraContent.querySelectorAll('.field.error')).map((item) =>
        {
            if (item.querySelector('label').querySelector('small').innerText)
                fieldName.push( '<b>' + item.querySelector('label').querySelector('small').innerText + '</b>');
        });

        return fieldName && fieldName.join(', ');
    }

    /**
     *
     */
    function addCheckFormFields()
    {
        $(form).change( function(element)
        {
            if ( !element.target.hasAttribute('required') )
                return false;
            else
                hideConfirm();
        });

        Array.from(form.querySelectorAll('[required="required"]')).map((item) => {

            item.addEventListener('click', hideConfirm );
        });
    }

    function hideConfirm()
    {
        checkValidForm();
        confirmSaveButton.style.display = 'none';
        saveButton.classList.remove( standartButtonDisableClass );
    }

    function getLeadForm()
    {
        add2Cache();
        //toggleExtra( extraButton.classList.contains('opened') ? 'closed' : 'opened');
    }

    function hideSaveLead()
    {
        if( confirmSaveButton.style.display === 'none' ) return false;
        confirmSaveButton.style.display = 'none';
        saveButton.classList.remove( standartButtonDisableClass );
    }

    function toggleSaveLead()
    {
        if ( !checkValidForm() ) return false;

        if( confirmSaveButton.style.display !== 'none')
        {
            confirmSaveButton.style.display = 'none';
            saveButton.classList.remove( standartButtonDisableClass );
        }
        else
        {
            confirmSaveButton.style.display = 'inline-block';
            saveButton.classList.add( standartButtonDisableClass );
        }

        hideAlert();
    }

    function saveLeadForm()
    {
        if ( !checkValidForm() ) return false;

        if(!saveButton.classList.contains('ui-btn-clock'))
        {
            saveButton.classList.add('ui-btn-clock');

            toggleSaveLead();

            sendPost( {command:saveLeadForm.name} );
        }
        else
        {
            saveButton.classList.remove('ui-btn-clock');
        }
    }

    function togglePopSize( command = 'opened' )
    {
        getPopSize();
        if(command === 'opened')
        {
            callTopLevel.style.width = mainWidth * 2 + 'px';
            extraButtonContainer.style.display = 'block';
        }
        else
        {
            callTopLevel.style.width = mainWidth / 2 + 'px';
            extraButtonContainer.style.display = 'none';
        }
    }

    function getPopSize()
    {
        mainWidth = parseInt(window.getComputedStyle(main, null).getPropertyValue("width"));
        mainHeight = parseInt(window.getComputedStyle(main, null).getPropertyValue("height"));
        buttonsContainerHeight = parseInt(window.getComputedStyle(buttonsContainer, null).getPropertyValue("height"));
    }

    function getWindowSize()
    {
        windowWidth = parseInt(window.innerWidth);
        windowHeight = parseInt(window.innerHeight);

        //console.log( windowHeight );
    }

    function toggleExtra( command = 'opened' )
    {
        if(!extraContent) return false;

        //togglePopSize(command);
        //fixExtraSize();
        //popupCenterPosition();
        //changeOverlay();

        //extraContent.style.height = mainHeight + 'px';
        //extraContent.style.display = command === 'opened' ? 'block' : 'none';
        //extraButton.classList.remove( command === 'opened' ? 'closed' : 'opened' );
        //extraButton.classList.add( command === 'opened' ? 'opened' : 'closed' );
    }

    function fixExtraSize()
    {
        //extraContentHeight = mainHeight - buttonsContainerHeight;
        //extraContent.style.maxHeight = extraContentHeight + 'px';
        //alertSection.style.bottom = buttonsContainerHeight + 'px';
    }

    function fixSearchResultSize()
    {
        let
            category = main.querySelector('.category.search'),
            categoryInput = category.querySelector('.input'),
            categoryResults = category.querySelector('.results'),
            categoryBottom = +categoryInput.getBoundingClientRect().bottom,
            extraButtonContainerTop = +extraButtonContainer.getBoundingClientRect().top
            //buttons = category.querySelectorAll('.entityOpen')
        ;

        categoryResults.style.height = (extraButtonContainerTop - categoryBottom - 28) + 'px';
        categoryResults.style.overflowY = 'auto';
        setup.fixSearchResultSize = true;

        //fixButtonsClick(buttons);
    }

    function fixDropdownMenu()
    {
        let
            category = main.querySelector('.drpdwn'),
            categoryResults = category.querySelector('.menu'),
            categoryBottom = +category.getBoundingClientRect().bottom,
            extraButtonContainerTop = +extraButtonContainer.getBoundingClientRect().top
        ;

        categoryResults.style.height = (extraButtonContainerTop - categoryBottom - 28) + 'px';
        categoryResults.style.overflowY = 'auto';
    }

    function fixButtonsClick( buttons )
    {
        let mark = 'marked';
        if ( !buttons )
            buttons = main.querySelectorAll('.entityOpen');

        Array.from(buttons).map((item) =>
        {
            if( !item.getAttribute('href') )    return;
            if( item && mark && item.classList.contains(mark) ) return;

            item.classList.add(mark);

            item.addEventListener('click', function()
            {
                event.preventDefault();
                event.stopPropagation();

                //minimizeButton.click();

                BX.Crm.Page.openSlider( item.getAttribute('href') );

                return false;
            });
        });
    }

    function fixScrollFieldSize()
    {
        let
            scrollFieldTop = scrollField && + scrollField.getBoundingClientRect().top,
            extraButtonContainerTop = extraButtonContainer && + extraButtonContainer.getBoundingClientRect().top
        ;

        scrollField.style.height = (extraButtonContainerTop - scrollFieldTop - 20) + 'px';
        scrollField.style.overflowY = 'auto';
        scrollField.style.overflowX = 'hidden';
    }

    function fixAlertPosition()
    {
        if ( alertSection && extraButtonContainer )
            alertSection.style.bottom = window.getComputedStyle(extraButtonContainer, null).getPropertyValue("height");
    }

    function fixAutoComplete()
    {
        Array.from(form.querySelectorAll('input')).map((item) =>
        {
            if (!item.getAttribute('name')) return;
            item.setAttribute('autocomplete', 'nope');

        });
    }

    function popupCenterPosition()
    {
        getPopSize();
        getWindowSize();
        main.style.left = ((windowWidth - mainWidth) / 2) + 'px';
    }

    function changeOverlay( index = 1000 )
    {
        main.style.zIndex = index ? index : mainDefaultZindex;
        //mainOverlay.style.zIndex = (index) ? index - 1 : mainOverlayDefaultZindex;
    }

    function toggleAlert( type = 'success', text = 'Someone will say romance about the blue sky...' )
    {
        alertCurentClass = 'ui-alert-' + type;
        alertSection.querySelector('.ui-alert').classList.add(alertCurentClass);
        alertSection.querySelector('.ui-alert-message').innerHTML = text;
        alertSection.style.display = 'block';
        fixAlertPosition();
        fixButtonsClick();
        setTimeout(hideAlert, delay);
    }

    function hideAlert()
    {
        alertSection.style.display = 'none';
        saveButton.classList.remove('ui-btn-clock');
        alertSection.querySelector('.ui-alert').classList.remove(alertCurentClass);
    }

    function sendPost( pref = {command} )
    {
        if( document.readyState !== 'complete' ) return false;

        if ( !saveButton.classList.contains('ui-btn-clock') )
            saveButton.classList.add('ui-btn-clock');

        saveButton.classList.add(standartButtonDisableClass);

        toogleAdditional();
        showAdditionalFoder( defaultAdditionalActive );
        removeButtonActives();

        //main.style.opacity = '0.8';
        form.classList.add('loading');
        BX.ajax({

            'url': setup.COMPONENT_PATH + '/ajax.php?' + pref.command,
            'method': 'post',
            'data': serialize(form),
            'dataType': 'json', //html|json|script – данные какого типа предполагаются в ответе
            'timeout': 300,
            'async': true,
            //'processData': true,
            //'scriptsRunFirst': true,
            //'emulateOnload': true,
            //'start': true, //|false – отправить ли запрос сразу или он будет запущен вручную
            'cache': false, // – в случае значения false к параметру URL будет добавляться случайный кусок, чтобы избежать браузерного кэширования
            'onsuccess': function (data, x, v)
            {
                if( data.error )
                    serverError( data );

                if ( data.success )
                    serverSuccess( data );
            },

            'onfailure': function(data, x){

                serverError( data );
            }
        });
    }

    function serverSuccess( data )
    {
        let message = 'Data saved successfully!';

        if ( data && data.message )
            message = data.message;

        hideSelectedCard();
        toggleAlert( 'success', message );

        saveButton.classList.remove('ui-btn-clock');
        saveButton.classList.remove(standartButtonDisableClass);
        form.classList.remove('loading');

        $('.ui.toggle.checkbox').checkbox('uncheck');

        add2Cache();

        fixButtonsClick();
    }

    function serverError( data )
    {
        let message = '<strong>Error accessing the server</strong>. Try again later or contact your system administrator ...';
        if ( data && data.message )
            message = data.message;
        else if ( data.type && data.data )
            message = data.data;

        toggleAlert('danger', message);

        saveButton.classList.remove('ui-btn-clock');
        saveButton.classList.remove(standartButtonDisableClass);
        form.classList.remove('loading');

        fixButtonsClick();
    }

    let serialize = function (form)
    {
        // Setup our serialized data
        let serialized = [];

        // Loop through each field in the form
        for (let i = 0; i < form.elements.length; i++)
        {
            let field = form.elements[i];
            if (!field.name || field.disabled || field.type === 'file' || field.type === 'reset' || field.type === 'submit' || field.type === 'button') continue;

            if (field.type === 'select-multiple') {
                for (let n = 0; n < field.options.length; n++) {
                    if (!field.options[n].selected) continue;
                    serialized.push(encodeURIComponent(field.name) + "=" + encodeURIComponent(field.options[n].value));
                }
            }
            else if ((field.type !== 'checkbox' && field.type !== 'radio') || field.checked) {
                serialized.push(encodeURIComponent(field.name) + "=" + encodeURIComponent(field.value));
            }
        }

        return serialized.join('&');
    };

    function removeButtonActives()
    {
        Array.from(form.getElementsByTagName('BUTTON')).map((item) =>
        {
            if (!item.getAttribute('id')) return;
            if( item && standartButtonDisableClass && item.classList.contains( standartButtonDisableClass )) return;
            if( item && standartButtonClass && item.classList.contains(standartButtonClass)) return;

            item.classList.remove(standartButtonActiveClass);
            item.classList.add(standartButtonClass);

        });
    }

    function toggleButtonActive( button = false )
    {
        if ( !button ) return;
        if ( button && standartButtonActiveClass && button.classList.contains(standartButtonClass) )
        //if ( $(button).hasClass(standartButtonClass) )
        {
            button.classList.remove(standartButtonClass);
            button.classList.add(standartButtonActiveClass);
            return true;
        }

        //if ( $(button).hasClass(standartButtonClass) )
        if ( button && standartButtonActiveClass && button.classList.contains(standartButtonActiveClass) )
        {
            button.classList.add(standartButtonClass);
            button.classList.remove(standartButtonActiveClass);
            return false;
        }
    }

    function openAdditional( folder = false )
    {
        if( additionalContainer.style.display !== 'block')
        {
            additionalContainer.style.display = 'block';
        }

        if( openingContainer.style.display !== 'none')
        {
            openingContainer.style.display = 'none';
        }

        hideAdditionalFolders();
        showAdditionalFoder(folder);
    }

    function toogleAdditional( folder = false )
    {
        if( additionalContainer.style.display !== 'none' )
        {
            additionalContainer.style.display = 'none';
            openingContainer.style.display = 'block';
            hideAdditionalFolders();
        }
        else
        {
            additionalContainer.style.display = 'block';
            openingContainer.style.display = 'none';
            showAdditionalFoder(folder);
        }
    }

    function showAdditionalFoder( folder = false )
    {
        if ( !folder ) return false;

        if ( currentAdditionalActive === folder )
        {
            toogleAdditional();
            currentAdditionalActive = defaultAdditionalActive;
            toggleButtonActive(currentButtonActive);
            return false;
        }

        let content = additionalContainer.querySelector('.' + folder + 'Content');

        if ( !content ) return false;

        if( content && standartButtonDisableClass && content.classList.contains( standartButtonDisableClass )) return false;

        if( content.style.display !== 'none' ) return false;

        content.style.display = 'block';

        currentAdditionalActive = folder;

        action.value = currentAdditionalActive;

        // $(".ui.drpdwn, .rmt.ui")
        //     .dropdown('refresh');
    }

    function hideAdditionalFolders()
    {
        Array.from(additionalContainer.querySelectorAll('[type="folder"]')).map((item) =>
        {
            item.style.display = 'none';
        });
    }

    function getAdditional()
    {
        event.preventDefault();

        if ( !event.target.id ) return;
        if ( standartButtonDisableClass && event.target.classList.contains( standartButtonDisableClass )) return;

        hideSaveLead(); // ok

        removeButtonActives(); // ok

        toggleButtonActive(event.target); // ok

        currentButtonActive = event.target;

        openAdditional(event.target.id); // ok
    }

    function defaultRestoreForm()
    {
        Array.from(form.getElementsByTagName('INPUT')).map((item) =>
        {
            if (!item.getAttribute('name')) return;

            item.value = cached[ item.getAttribute('name') ];

        });

        $('.ui.drpdwn, .ui.remote, .ui.search').dropdown('restore default value');

        $('.ui.toggle.checkbox').checkbox('uncheck');
    }

    function add2Cache( update = false )
    {
        Array.from(form.getElementsByTagName('INPUT')).map((item) =>
        {
            if (!item.getAttribute('name')) return;
            if ( item.getAttribute('name') === 'ASSIGNED_BY_ID' )
            {
                // let u = BX.Main.User.Selector('ASSIGNED_BY_ID').getUser();
                // console.log(u);
            }

            //console.log(item.getAttribute('name') in cached);

            if ( !(item.getAttribute('name') in cached) || update )
                cached[ item.getAttribute('name') ] = item.value;
        });

        //console.log( cached );
    }

    // fixing an error of a disabled opening button
    function startMutations()
    {
        mutationObserver = new MutationObserver(function(mutations) {
            mutations.forEach(function(mutation)
            {
                if( mutation.type !== 'childList' ) return;

                fixButtonsClick();

            });
        });

        mutationObserver
            .observe(main.querySelector('.category.search > .results'),
                {
                    attributes: true,
                    characterData: true,
                    childList: true,
                    subtree: false,
                    attributeOldValue: false,
                    characterDataOldValue: false
                })
        ;
    }

    function stopMutations()
    {
        mutationObserver.disconnect();
    }
}

function Base64Encode(str, encoding = 'utf-8') {
    let bytes = new (TextEncoder || TextEncoderLite)(encoding).encode(str);
    return base64js.fromByteArray(bytes);
}

function Base64Decode(str, encoding = 'utf-8') {
    let bytes = base64js.toByteArray(str);
    return new (TextDecoder || TextDecoderLite)(encoding).decode(bytes);
}

(function(r){if(typeof exports==="object"&&typeof module!=="undefined"){module.exports=r()}else if(typeof define==="function"&&define.amd){define([],r)}else{var e;if(typeof window!=="undefined"){e=window}else if(typeof global!=="undefined"){e=global}else if(typeof self!=="undefined"){e=self}else{e=this}e.base64js=r()}})(function(){var r,e,n;return function(){function d(a,f,i){function u(n,r){if(!f[n]){if(!a[n]){var e="function"==typeof require&&require;if(!r&&e)return e(n,!0);if(v)return v(n,!0);var t=new Error("Cannot find module '"+n+"'");throw t.code="MODULE_NOT_FOUND",t}var o=f[n]={exports:{}};a[n][0].call(o.exports,function(r){var e=a[n][1][r];return u(e||r)},o,o.exports,d,a,f,i)}return f[n].exports}for(var v="function"==typeof require&&require,r=0;r<i.length;r++)u(i[r]);return u}return d}()({"/":[function(r,e,n){"use strict";n.byteLength=f;n.toByteArray=i;n.fromByteArray=p;var u=[];var v=[];var d=typeof Uint8Array!=="undefined"?Uint8Array:Array;var t="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";for(var o=0,a=t.length;o<a;++o){u[o]=t[o];v[t.charCodeAt(o)]=o}v["-".charCodeAt(0)]=62;v["_".charCodeAt(0)]=63;function c(r){var e=r.length;if(e%4>0){throw new Error("Invalid string. Length must be a multiple of 4")}var n=r.indexOf("=");if(n===-1)n=e;var t=n===e?0:4-n%4;return[n,t]}function f(r){var e=c(r);var n=e[0];var t=e[1];return(n+t)*3/4-t}function h(r,e,n){return(e+n)*3/4-n}function i(r){var e;var n=c(r);var t=n[0];var o=n[1];var a=new d(h(r,t,o));var f=0;var i=o>0?t-4:t;var u;for(u=0;u<i;u+=4){e=v[r.charCodeAt(u)]<<18|v[r.charCodeAt(u+1)]<<12|v[r.charCodeAt(u+2)]<<6|v[r.charCodeAt(u+3)];a[f++]=e>>16&255;a[f++]=e>>8&255;a[f++]=e&255}if(o===2){e=v[r.charCodeAt(u)]<<2|v[r.charCodeAt(u+1)]>>4;a[f++]=e&255}if(o===1){e=v[r.charCodeAt(u)]<<10|v[r.charCodeAt(u+1)]<<4|v[r.charCodeAt(u+2)]>>2;a[f++]=e>>8&255;a[f++]=e&255}return a}function s(r){return u[r>>18&63]+u[r>>12&63]+u[r>>6&63]+u[r&63]}function l(r,e,n){var t;var o=[];for(var a=e;a<n;a+=3){t=(r[a]<<16&16711680)+(r[a+1]<<8&65280)+(r[a+2]&255);o.push(s(t))}return o.join("")}function p(r){var e;var n=r.length;var t=n%3;var o=[];var a=16383;for(var f=0,i=n-t;f<i;f+=a){o.push(l(r,f,f+a>i?i:f+a))}if(t===1){e=r[n-1];o.push(u[e>>2]+u[e<<4&63]+"==")}else if(t===2){e=(r[n-2]<<8)+r[n-1];o.push(u[e>>10]+u[e>>4&63]+u[e<<2&63]+"=")}return o.join("")}},{}]},{},[])("/")});