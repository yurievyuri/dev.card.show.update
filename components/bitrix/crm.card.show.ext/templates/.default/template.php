<?php
use \Bitrix\Main\Config\Option;
use \Dev\Call\Card;
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

if( !class_exists('\Dev\Call\Card') )
    \Bitrix\Main\Loader::includeModule('dev.card.show');
$module_id = \Dev\Call\Card::module;
$prefix = \Dev\Call\Card::prefix;

\Bitrix\Main\UI\Extension::load("ui.alerts");
\Bitrix\Main\UI\Extension::load("ui.icons.service");
\Bitrix\Main\UI\Extension::load("ui.buttons");
\Bitrix\Main\UI\Extension::load("ui.buttons.icons");
//\Bitrix\Main\Page\Asset::getInstance()->addCss('/bitrix/js/crm/css/crm.css');
//\Bitrix\Main\Page\Asset::getInstance()->addCss("/bitrix/templates/bitrix24/interface.css");

////\Bitrix\Main\UI\Extension::load("ui.selector");
////\Bitrix\Main\UI\Extension::load("ui.dropdown");

\Bitrix\Main\Page\Asset::getInstance()->addCss( $templateFolder . '/semantic/semantic.css' );
//\Bitrix\Main\Page\Asset::getInstance()->addCss( '/bitrix/components/bitrix/crm.entity.editor/templates/.default/style.css' );
\Bitrix\Main\Page\Asset::getInstance()->addCss( '/bitrix/js/im/css/phone_call_view.css' );

////\Bitrix\Main\Page\Asset::getInstance()->addCss( $templateFolder . '/fa5-all.css' );
////\Bitrix\Main\Page\Asset::getInstance()->addJs( $templateFolder . '/fa5-all.js' );

\Bitrix\Main\Page\Asset::getInstance()->addJs($templateFolder . '/jquery.min.js');
\Bitrix\Main\Page\Asset::getInstance()->addJs( $templateFolder . '/semantic/semantic.js' );

\Bitrix\Main\Page\Asset::getInstance()->addJs('/bitrix/js/crm/common.js');
//\Bitrix\Main\Page\Asset::getInstance()->addJs('/bitrix/js/crm/progress_control.js');

if ( $component->getErrors() )
     echo implode('<br>',$component->getErrors());

$globalSize = 'sm';
$globalIcon = 'fas';
$globalButtonClasses = 'ui-btn ui-btn-'.$globalSize.' ui-btn-round ui-btn-no-caps';

//if( $arParams['DEBUG'] )
//    \Bitrix\Main\Diag\Debug::writeToFile($arResult['ENTITY_DATA'], '<-------' .date('d.m.Y H:i:s') , 'entityData.log');

//$arParams['ID'] = $arResult['ENTITY_DATA']['ID'];
//$arParams['TYPE'] = $arResult['ENTITY_DATA']['ENTITY_TYPE'];
//echo '<pre>'; print_r( $arResult ); echo '</pre>';

if($arResult['DEFAULT_PHONES'])
    $arParams['DEFAULT_PHONES'] = $arResult['DEFAULT_PHONES'];
$arParams['COMPONENT_PATH'] = $componentPath;
$arParams['TEMPLATE_FOLDER'] = $templateFolder;
$arParams['BITRIX_SESSID'] = bitrix_sessid();
$arParams['ACTION'] = 'opening';

$additionalButtons = [
   'link'       => 'ui-btn-light-border ui-btn-disabled|users icon',   //coming soon
   'relative'   => 'ui-btn-light-border ui-btn-disabled',   //coming soon
   'contact'    => 'ui-btn-light-border|address card outline icon',   //coming soon
   'existing'   => 'ui-btn-light-border|cut icon',
   'junk'       => 'ui-btn-light-border|trash icon'
];
?>
<!--<script src="//api.bitrix24.com/api/v1/"></script>-->
<div class="">

    <div id="im-phone-call-view-extra" class="ui container">

        <form class="ui form">

            <?=CrmCardShowExtComponent::getHiddenInputs($arParams);?>

            <? require $_SERVER['DOCUMENT_ROOT'] . $templateFolder . '/elements/search.php' ;?>

            <?if( $arParams['ENTITY_TYPE'] == 'LEAD'):?>

                <? require $_SERVER['DOCUMENT_ROOT'] . $templateFolder . '/elements/buttons.php' ;?>

                <div class="scrollField">
                    <div class="<?=$arParams['ACTION'];?>" type="folder">
                        <? require $_SERVER['DOCUMENT_ROOT'] . $templateFolder . '/elements/inputs_ui.php' ;?>

                        <?require_once $_SERVER['DOCUMENT_ROOT'] . $templateFolder . '/elements/history.php' ;?>
                    </div>
                    <div class="additional" style="display:none;">
                        <? require $_SERVER['DOCUMENT_ROOT'] . $templateFolder . '/elements/additional.php' ;?>
                    </div>
                </div>

            <?else:?>

                <div class="ui negative message">
                    <div class="header">
                        Call associated with contact
                    </div>
                    <p>advanced management is possible only with leads</p>
                </div>

            <?endif;?>

            <div class="ui error message"></div>

        </form>
    </div>

    <div id="alertSection">
        <div class="ui-alert ui-alert-success ui-alert-sm ui-alert-icon-info ui-alert-close-animate ui-alert-text-center" style="margin-bottom:0;padding:32px 34px 31px 18px;">
            <span class="ui-alert-message"></span>
            <span class="ui-alert-close-btn"></span>
        </div>
    </div>

    <div id="extraButtonContainer" class="im-phone-call-buttons-container">
        <span id="defaultLead" class="ui-btn ui-btn-round ui-btn-light-border">Default</span>
        <span id="saveLead" class="ui-btn ui-btn-round ui-btn-primary-dark ui-btn-md">Save</span>
        <span style="display: none;" id="confirmSaveLead" class="ui-btn ui-btn-round ui-btn-success ui-btn-md ui-btn-icon-task">Confirm</span>
    </div>

</div>

<script>
    BX.ready(extraStart);
</script>