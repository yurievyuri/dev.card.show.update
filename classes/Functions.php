<?php


namespace Dev\Call;

use Bitrix\Crm\Automation\Converter\Converter;
use Bitrix\Crm\FieldMultiTable;

/**
 * Class Functions
 * @property array RESULT
 * @package Dev\Call
 */
class Functions extends Card
{
    /**
     * @var array
     */
    public $arRequest, $arResult;
    /**
     * @var
     */
    private $ERRORS;
    /**
     * @var string
     */
    private $currentAction;

    /**
     * Functions constructor.
     * @param array $request
     */
    public function __construct( $request = [] )
    {
        if ( !$request )
        {
            $this->setERROR( __METHOD__ , 'No valid input received');
            return [];
        }
        $this->arRequest = $request;
        $this->arResult = [];
    }

    /**
     * @return int
     */
    public function getPinValue()
    {
        $key = 'PIN_VALUE';

        if ( !$this->arRequest[$key] )
        {
            //$this->setERROR( __METHOD__ , 'Missing item id');
            //return [];
        }
        return (int)$this->arRequest[$key];
    }

    /**
     * @return mixed
     */
    public function getPinType()
    {
        $key = 'PIN_TYPE';
        if ( !$this->arRequest[$key] )
        {
            //$this->setERROR( __METHOD__ , 'Missing item type');
            //return [];
        }
        return $this->arRequest[$key];
    }

    /**
     * @param $id
     * @return array|bool
     */
    public function getContactData($id )
    {
        if ( !$this->arResult['CONTACT_DATA'][$id] )
        {
            $this->setERROR( __METHOD__ , 'Failed to get contact processing data.');
            return [];
        }
        return $this->arResult['CONTACT_DATA'][ $id ] ? : false;
    }

    /**
     * @param array $array
     * @return bool
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function checkDublicate(array $array ) : bool
    {
        $return = parent::Search( $array['VALUE'], $array['ENTITY_ID'], $array['ELEMENT_ID'] );

        return  $return ? false : true;
    }


    /**
     * @param bool $contactId
     * @param bool $actionType
     * @return array|void
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function actionExecute($contactId = false, $actionType = false )
    {
        if ( !$contactId || !$actionType )
        {
            $this->setERROR( __METHOD__ , 'Failed to get action data.');

            return [];
        }

        $array = $this->getContactData($contactId);

        unset( $array['ID'] );
        $array['ENTITY_ID'] = $this->getPinType();
        $array['ELEMENT_ID'] = $this->getPinValue();

        if ( $this->getERROR() ) return;

        if ( !class_exists('CCrmSearch'))
            \Bitrix\Main\Loader::includeModule('crm');

        $el = new \CCrmFieldMulti();

        if ( $actionType == 'ADD' && !empty( $array ) )
        {
            if( $this->checkDublicate($array) )
                $this->arResult['UPDATE'][$this->currentAction][$actionType][$this->getPinValue()] = $el->Add( $array, ['ENABLE_NOTIFICATION' => true] );

            if($el->LAST_ERROR)
                $this->setERROR(__METHOD__, $el->LAST_ERROR);
            //$this->arResult['UPDATE'][$this->currentAction][$actionType][$this->getPinValue()] = \Bitrix\Crm\FieldMultiTable::add($array);
        }
        elseif ( $actionType == 'DELETE' && $contactId )
        {
            $this->arResult['UPDATE'][$this->currentAction][$actionType][$this->getPinValue()] = $el->Delete( $contactId, ['ENABLE_NOTIFICATION' => true] );
            if($el->LAST_ERROR)
                $this->setERROR(__METHOD__, $el->LAST_ERROR);
            //$this->arResult['UPDATE'][$this->currentAction][ $actionType ][$contactId] = \Bitrix\Crm\FieldMultiTable::delete( $contactId );
        }
        else return [];

        if ( !$el->LAST_ERROR )
            \CCrmSearch::UpdateSearch(
                array(
                    'ID' => $this->getPinValue(),
                    'CHECK_PERMISSIONS' => 'N'
                ),
                $this->getPinType(),
                true
            );
    }

    /**
     * @return bool
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function makeOutActions()
    {
        if ( $this->getPinValue() < 0 || !$this->getPinValue() || !$this->getPinType()) return false;

        if ( $this->getPinValue() == 'undefined' )  return false;
        if ( $this->getPinType() == 'undefined' )   return false;

        $this->getExistDataContactById();

        foreach( $this->arResult['ACTION_FUNCTIONS'] as $entity_id => $array )
        {
            foreach ( $array as $contactId => $actionType )
            {
                if ( !$actionType ) continue;

                $this->actionExecute( $contactId, $actionType );
            }
        }
    }

    /**
     * @return array
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function getExistDataContactById()
    {
        $result = [];

        $db = \Bitrix\Crm\FieldMultiTable::getList([

            'filter'    => ['ID' => $this->getExtistFormContactId() ]

        ]);

        while ($item = $db->Fetch())
            $result[ $item['ID'] ] = $item;

        return $this->arResult['CONTACT_DATA'] = $result;
    }

    /**
     * @return array
     */
    function getExtistFormContactId()
    {
        $this->getCurrentAction();

        if ( !isset($this->arRequest[$this->currentAction]) )
        {
            $this->setERROR( __METHOD__ , 'Unable to retrieve data for processing.');
            return [];
        }

        $result = [];

        foreach( $this->arRequest[$this->currentAction] as $entity_type => $value )
        {
            foreach ( $value as $entity_id => $array )
            {
                $result = array_merge(array_keys($array), $result);
                $this->arResult['ACTION_FUNCTIONS'][ $entity_id ] = $array ? : false;
            }
        }

       return $this->arResult['CONTACT_ARRAY'] = array_unique($result);
    }

    /**
     * Automatic conversion of lead into contact with subsequent removal of lead
     * @return bool
     * @throws \Bitrix\Main\ArgumentTypeException
     * @throws \Bitrix\Main\LoaderException
     * @author yurievyuri 2020-02-18
     * @version 0.1
     */
    public function convertLeadToContact() : bool
    {
        if ( !$this->arRequest ) return false;

        if ( !isset($this->arRequest['MOVE_LEAD']) ) return false;

        $contactData = self::getEntityContacts( $this->arRequest['ENTITY_ID'] );
        $searchedData = self::searchContactDublicates( $contactData );

        if ( $searchedData['ENTITY_TOTAL'] > 0 )
        {
            $this->setERROR( false, 'Similar related contacts were found:<br>' . self::getContactsDataLink($searchedData) );
            return false;
        }

        $result = self::convertExecute($this->arRequest['ENTITY_ID']);

        if  ( $result['errors_check'] || $result['errors_text'] )
        {
            $this->setERROR( false, $result['errors_check'] );
            $this->setERROR( false, $result['errors_text'] );

            return false;
        }

        if ( $result['finished'] === true )
        {
            if ( $result['result_data']['IS_RECENT_CONTACT'] )
                $this->setRESULT( false, '<span class=entityOpen href='.$result['redirect_url'].'>Lead converted to <b>contact '.$result['result_data']['CONTACT'].'</b></span>' );

            elseif ( !$result['result_data']['IS_RECENT_CONTACT'] && $result['result_data']['CONTACT'] > 0 )
                $this->setRESULT( false, '<span class=entityOpen href='.$result['redirect_url'].'>Exist contact '.$result['result_data']['CONTACT'].' updated</span>' );
        }

        return true;
    }

    /**
     * Automatically convert lead to contact according to all Bitrix rules
     * @author yurievyuri
     * @param bool $entityId
     * @return array
     */
    static public function convertExecute( $entityId = false ) : array
    {
        if ( !$entityId ) return [];
        $checkErrors = array();

        $entity = new \CCrmLead(false);
        $fieldsForCheck = array('STATUS_ID' => 'CONVERTED');
        if(!$entity->CheckFields($fieldsForCheck, $entityId)) {
            $checkExceptions = $entity->GetCheckExceptions();
            foreach ($checkExceptions as $exception) {
                if ($exception instanceof \CAdminException) {
                    foreach ($exception->GetMessages() as $message) {
                        $checkErrors[$message['id']] = $message['text'];
                    }
                }
            }
        }

        $configData = [
            'deal'      => ['active' => 'N', 'enableSync' => 'N','initData' => ['categoryId' => 2]],
            'contact'   => ['active' => 'Y', 'enableSync'=> 'N'],
            'company'   => ['active' => 'N', 'enableSync'=> 'N'],
        ];
        $config = new \Bitrix\Crm\Conversion\LeadConversionConfig();
        $config->fromJavaScript($configData);

        //$config->setTypeID(\CCrmOwnerType::Contact);
        //$config = LeadConversionConfig::load();
        //if($config === null)
        //   $config = LeadConversionConfig::getDefault();
        //$entity = new \CCrmLead(false);
        //$fieldsForCheck = array();
        //$entity->CheckFields($fieldsForCheck, $entityID);

        \Bitrix\Crm\Conversion\LeadConversionWizard::remove($entityId);
        $wizard = new \Bitrix\Crm\Conversion\LeadConversionWizard($entityId, $config);
        $wizard->execute();

        return $result = [

            'errors_check' => $checkErrors,
            'errors_text' => $wizard->getErrorText(),
            'result_data' =>  $wizard->getResultData(),
            'redirect_url'  => $wizard->getRedirectUrl(),
            'finished'  => $wizard->isFinished()

        ];
    }

    /**
     * We get the data of the detected contacts in a human form
     * @param array $searchedData
     * @return string
     */
    public function getContactsDataLink( array &$searchedData = [] )
    {
        if ( $searchedData['ENTITY_TOTAL'] < 1 ) return '';

        foreach ( $searchedData['DUPLICATES'] as $value )
        {
            foreach ( $value['ENTITIES'] as $data )
            {
                $contacts = [];
                if ( $data['PHONE'])
                    foreach( $data['PHONE'] as $phone )
                    $contacts[] = parent::phoneFormatEx($phone);
                if ( $data['EMAIL'])
                    $contacts[] = implode(',', $data['EMAIL']);

                $searchedData['DETECTED'][] = '<span title=open class=entityOpen href='.$data['URL'].'><b>'.$data['TITLE'] .'</b>, ' . implode(', ',$contacts) . '</span>';
            }
        }

        return $searchedData['DETECTED'] ? implode('<br>',$searchedData['DETECTED']) : '';
    }

    /**
     * Getting all contacts of the entity to check for duplicates before conversion
     * @author yurievyuri
     * @param bool $entityId
     * @return array
     * @throws \Bitrix\Main\LoaderException
     */
    static public function getEntityContacts( $entityId = false ) : array
    {
        if ( !$entityId ) return [];

        if ( !class_exists('CCrmFieldMulti'))
            \Bitrix\Main\Loader::includeModule("crm");

        $result = [];
        $db = \CCrmFieldMulti::GetListEx(
            array(),
            array(
                'ELEMENT_ID' => $entityId,
                '@TYPE_ID' => array('PHONE', 'EMAIL')
            ),
            false,
            false,
            array('ELEMENT_ID', 'TYPE_ID', 'VALUE')
        );

        while ($list = $db->Fetch())
            $result['FM'][ $list['TYPE_ID'] ][] = [ 'VALUE' => $list['VALUE'] ];

        return $result;
    }

    /**
     * Free interpretation of the Bitrix method for finding duplicates
     * @param array $fields
     * @author bitrix24
     * @return array
     * @throws \Bitrix\Main\ArgumentTypeException
     * @throws \Bitrix\Main\LoaderException
     */
    static public function searchContactDublicates( array $fields = [] ) : array
    {
        if ( !class_exists('CCrmOwnerType'))
            \Bitrix\Main\Loader::includeModule("crm");

        if ( !$fields ) return [];

        $checker = new \Bitrix\Crm\Integrity\ContactDuplicateChecker();
        $fieldNames = ['FM.PHONE', 'FM.EMAIL'];

        $adapter = \Bitrix\Crm\EntityAdapterFactory::create($fields, \CCrmOwnerType::Contact);
        $dups = $checker->findDuplicates($adapter, new \Bitrix\Crm\Integrity\DuplicateSearchParams($fieldNames));

        $entityInfoByType = array();
        foreach($dups as &$dup)
        {
            if(!($dup instanceof \Bitrix\Crm\Integrity\Duplicate))
                continue;

            $entities = $dup->getEntities();
            if(!(is_array($entities) && !empty($entities)))
                continue;

            //Each entity type limited by 50 items
            foreach($entities as &$entity)
            {
                if(!($entity instanceof \Bitrix\Crm\Integrity\DuplicateEntity))
                    continue;

                $entityTypeID = $entity->getEntityTypeID();
                $entityID = $entity->getEntityID();

                // fixed contact entity
                if ( $entityTypeID != \CCrmOwnerType::Contact )
                    continue;

                if(!isset($entityInfoByType[$entityTypeID]))
                    $entityInfoByType[$entityTypeID] = array($entityID => array());
                elseif(count($entityInfoByType[$entityTypeID]) < 50 && !isset($entityInfoByType[$entityTypeID][$entityID]))
                    $entityInfoByType[$entityTypeID][$entityID] = array();
            }
        }

        $totalEntities = 0;
        $entityMultiFields = array();
        foreach($entityInfoByType as $entityTypeID => &$entityInfos)
        {
            $totalEntities += count($entityInfos);
            \CCrmOwnerType::PrepareEntityInfoBatch(
                $entityTypeID,
                $entityInfos,
                false,
                array(
                    'ENABLE_RESPONSIBLE' => false,
                    'ENABLE_EDIT_URL' => true,
                )
            );

            $multiFieldResult = \CCrmFieldMulti::GetListEx(
                array(),
                array(
                    '=ENTITY_ID' => \CCrmOwnerType::ResolveName($entityTypeID),
                    '@ELEMENT_ID' => array_keys($entityInfos),
                    '@TYPE_ID' => array('PHONE', 'EMAIL')
                ),
                false,
                false,
                array('ELEMENT_ID', 'TYPE_ID', 'VALUE')
            );

            if(is_object($multiFieldResult))
            {
                $entityMultiFields[$entityTypeID] = array();
                while($multiFields = $multiFieldResult->Fetch())
                {
                    $entityID = isset($multiFields['ELEMENT_ID']) ? intval($multiFields['ELEMENT_ID']) : 0;
                    if($entityID <= 0)
                        continue;

                    if(!isset($entityMultiFields[$entityTypeID][$entityID]))
                        $entityMultiFields[$entityTypeID][$entityID] = array();

                    $typeID = isset($multiFields['TYPE_ID']) ? $multiFields['TYPE_ID'] : '';
                    $value = isset($multiFields['VALUE']) ? $multiFields['VALUE'] : '';
                    if($typeID === '' || $value === '')
                        continue;

                    if(!isset($entityMultiFields[$entityTypeID][$entityID][$typeID]))
                        $entityMultiFields[$entityTypeID][$entityID][$typeID] = array($value);
                    elseif(count($entityMultiFields[$entityTypeID][$entityID][$typeID]) < 10)
                        $entityMultiFields[$entityTypeID][$entityID][$typeID][] = $value;
                }
            }
        }
        unset($entityInfos);
        $dupInfos = array();
        foreach($dups as &$dup)
        {
            if(!($dup instanceof \Bitrix\Crm\Integrity\Duplicate))
                continue;

            $entities = $dup->getEntities();
            $entityCount = is_array($entities) ? count($entities) : 0;
            if($entityCount === 0)
                continue;

            $dupInfo = array('ENTITIES' => array());
            foreach($entities as &$entity)
            {
                if(!($entity instanceof \Bitrix\Crm\Integrity\DuplicateEntity))
                    continue;

                $entityTypeID = $entity->getEntityTypeID();
                // fixed contact entity
                if ( $entityTypeID != \CCrmOwnerType::Contact )
                    continue;

                $entityID = $entity->getEntityID();

                $info = array(
                    'ENTITY_TYPE_ID' => $entityTypeID,
                    'ENTITY_ID' => $entityID
                );

                if(isset($entityInfoByType[$entityTypeID]) && isset($entityInfoByType[$entityTypeID][$entityID]))
                {
                    $entityInfo = $entityInfoByType[$entityTypeID][$entityID];
                    if(isset($entityInfo['TITLE']))
                        $info['TITLE'] = $entityInfo['TITLE'];
                    $entityTypeName = \CCrmOwnerType::ResolveName($entityTypeID);
                    $isReadable = \CCrmAuthorizationHelper::CheckReadPermission($entityTypeName, $entityID, $userPermissions);
                    $isEditable = \CCrmAuthorizationHelper::CheckUpdatePermission($entityTypeName, $entityID, $userPermissions);

                    if($isEditable && isset($entityInfo['EDIT_URL']))
                        $info['URL'] = $entityInfo['EDIT_URL'];
                    elseif($isReadable && isset($entityInfo['SHOW_URL']))
                        $info['URL'] = $entityInfo['SHOW_URL'];
                    else
                        $info['URL'] = '';
                }

                if(isset($entityMultiFields[$entityTypeID])
                    && isset($entityMultiFields[$entityTypeID][$entityID]))
                {
                    $multiFields = $entityMultiFields[$entityTypeID][$entityID];
                    if(isset($multiFields['PHONE']))
                        $info['PHONE'] = $multiFields['PHONE'];
                    if(isset($multiFields['EMAIL']))
                        $info['EMAIL'] = $multiFields['EMAIL'];
                }

                $dupInfo['ENTITIES'][] = &$info;
                unset($info);
            }
            unset($entity);

            $criterion = $dup->getCriterion();
            if($criterion instanceof \Bitrix\Crm\Integrity\DuplicateCriterion)
            {
                $dupInfo['CRITERION'] = array(
                    'TYPE_NAME' => $criterion->getTypeName(),
                    'MATCHES' => $criterion->getMatches()
                );
            }
            $dupInfos[] = &$dupInfo;
            unset($dupInfo);
        }
        unset($dup);
        return array(
            'DUPLICATES' => &$dupInfos,
            'ENTITY_TOTAL_TEXT' => \Bitrix\Crm\Integrity\Duplicate::entityCountToText($totalEntities),
            'ENTITY_TOTAL'  => $totalEntities
        );
    }


    /**
     * Hiding the lead by request from the application (I think deleting it completely is not a good idea)
     * @return mixed
     * @author yurievyuri
     */
    public function deleteLead()
    {
        if ( !isset($this->arRequest['DELETE_LEAD'])
            && !isset($this->arRequest['MOVE_LEAD']) )
            return false;

        $entity = new \CCrmLead(false);
        $entity->Delete( $this->arRequest['ENTITY_ID'] );
        return $entity->LAST_ERROR
            ? $this->setERROR( false, $entity->LAST_ERROR )
            : $this->setRESULT( false, 'Lead successfully deleted...');
    }

    /**
     * @return array|string
     */
    public function getCurrentAction()
    {
        if ( !$this->arRequest['ACTION'] ) {
            $this->setERROR( __METHOD__ , 'No action specified in the dataset');
            return [];
        }
        return $this->currentAction = strtoupper($this->arRequest['ACTION']);
    }

    /**
     * @param bool $method
     * @param bool $error
     * @return bool
     */
    public function setERROR( $method = false, $error = false )
    {
        if ( !$error ) return false;
        if ( $method )
            $this->ERRORS[] = $method . ' :: ' . $error;
        else
            $this->ERRORS[] = $error;

        return true;
    }

    /**
     * @return mixed
     */
    public function getERROR()
    {
        return $this->ERRORS;
    }

    /**
     * @return mixed
     */
    public function lastERROR()
    {
        return end( $this->ERRORS );
    }

    public function setRESULT( $method = false, $result = false )
    {
        if ( !$result ) return false;
        if ( $method )
            $this->RESULT[] = $method . ' :: ' . $result;
        else
            $this->RESULT[] = $result;

        return true;
    }

    /**
     * @return mixed
     */
    public function getRESULT()
    {
        return $this->RESULT;
    }
}