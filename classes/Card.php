<?php
namespace Dev\Call;
use Bitrix\Main\Config\Option;

/**
 * @property bool init
 */
class Card {

    /**
     *
     */
    const defaultCardTemplatePath = '/bitrix/components/bitrix/crm.card.show/templates/.default/template.php';
    const module = 'dev.card.show';
    const prefix = 'ahc_card_';
    const placement = 'CALL_CARD';
    const appname = self::module; //'AHC CALL EXTRA HANDLER';
    const appcode = self::module.self::placement;

    public function __construct( $option = [] )
    {
        $this->init = true;
    }

    /**
     * @param bool $search
     * @param string $type
     * @param bool $elementId
     * @return array
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public static function Search($search = false, $type = 'CONTACT', $elementId = false )
    {
        $entityFilterIdArray = [];
        $contactArray = [];
        $contactDataArray = [];
        $searchedNamesIdArray = [];

        if ( $elementId === false )
        {
            $result = \Bitrix\Main\UI\Selector\Entities::search([], self::entityTypes(), array(
                'searchString' => $search,
                'searchStringConverted' => '',
                //'additionalData' => $additionalData
            ))['ENTITIES'];

            foreach ($result as $code => $value)
            {
                if ($code == 'CONTACTS')
                    $key = 'CONTACT';
                elseif ($code == 'COMPANIES')
                    $key = 'COMPANY';
                elseif ($code == 'LEADS')
                    $key = 'LEAD';
                elseif ($code == 'DEALS')
                    $key = 'DEAL';
                else continue;
                if (!$value['ITEMS']) continue;
                foreach ($value['ITEMS'] as $k => $v)
                {
                    $namesArray[$key][$v['entityId']] = $v;
                    $searchedNamesIdArray[$key][] = $v['entityId'];
                }
            }
        }

        $filterLogic = [
            'LOGIC' => 'OR',
            ['VALUE'  => $search],
            ['%VALUE' => '%'.$search.'%' ]
        ];

        if ( $searchedNamesIdArray[$type] && !$elementId )
            $filterLogic[]['ELEMENT_ID'] = $searchedNamesIdArray[$type];

        $phoneString = preg_replace("/[^\d]/", '', $search);
        if ( $phoneString )
        {
            $res = \Bitrix\Main\PhoneNumber\Parser::getInstance()->parse( $phoneString );
            $filterLogic[]['=VALUE'] = self::phoneFormat($search);
            $filterLogic[]['VALUE'] = $res->getRawNumber();
            //$filterLogic[]['VALUE'] = $res->format(\Bitrix\Main\PhoneNumber\Format::INTERNATIONAL);
            $filterLogic[]['VALUE'] = $res->format(\Bitrix\Main\PhoneNumber\Format::E164);
            $filterLogic[]['VALUE'] = $res->format(\Bitrix\Main\PhoneNumber\Format::NATIONAL);

            if (!$elementId)
                $filterLogic[]['ELEMENT_ID'] = $phoneString;
        }

        $filterMain = [
            'ENTITY_ID' => [ $type ],
            $filterLogic
        ];
        if ( $elementId )
            $filterMain['ELEMENT_ID'] = $elementId;

        $db = \Bitrix\Crm\FieldMultiTable::getList([
            'select' => ['*'], 'filter' => $filterMain, 'limit' => 20
        ]);

        // first step
        while ( $item = $db->fetch() ) {
            $entityFilterIdArray[$item['ENTITY_ID']][$item['ID']] = $item['ELEMENT_ID'];
            $contactDataArray[ $item['ELEMENT_ID'] ][$item['ID']] = $item;
        }

        $db = \Bitrix\Crm\FieldMultiTable::getList([
            'select' => ['*'], 'filter' => [
                'ELEMENT_ID' => $entityFilterIdArray[ $type ],
                '!=ID' => key($entityFilterIdArray[ $type ])
            ]
        ]);
        // step second
        while ( $item = $db->fetch() ) {
            $entityFilterIdArray[$item['ENTITY_ID']][$item['ID']] = $item['ELEMENT_ID'];
            $contactDataArray[ $item['ELEMENT_ID'] ][$item['ID']] = $item;
        }

        foreach ( $entityFilterIdArray as $entityId => $idArray )
        {
            if ($entityId == 'CONTACT')
                $ob = new \CCrmContact(false); //new Crm\ContactTable();
            elseif ($entityId == 'LEAD')
                $ob = new \CCrmLead(false); //$ob = new Crm\LeadTable();
            elseif ($entityId == 'DEAL')
                $ob = new \CCrmDeal(false); //$ob = new Crm\DealTable();
            elseif ($entityId == 'COMPANY')
                $ob = new \CCrmCompany(false); //$ob = new Crm\CompanyTable();
            else continue;

            if ( $searchedNamesIdArray[$entityId] )
                $idArray = array_merge($idArray,$searchedNamesIdArray[$entityId]);

            // oldschool format
            $db = $ob->GetList(['NAME' => 'ASC'],['ID' => array_unique($idArray)]);

            // D7 format
            /*$db = $ob::getList(['select' => ['*'],'filter' => ['ID' => array_unique($idArray)]]);*/

            while ($item = $db->fetch())
            {
                $item['SEARCHED'] = $contactDataArray[$item['ID']];
                $contactArray[$entityId][$item['ID']] = $item;
            }
        }

        return $contactArray;
    }


    /**
     * @param bool $search
     * @param array $array
     * @param string $type
     * @return array
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public static function prepareArray($search = false, $array = [], $type = 'CONTACT' )
    {
        if ( !trim($search) && !$array )
            return [];

        if ( !$array )
            $array = self::Search( trim($search), $type );

        $result = [];
        foreach( $array as $type => $v )
        {
            if ( $type == 'CONTACT' )
                $category = 'Contacts';
            elseif ( $type == 'LEAD' )
                $category = 'Leads';
            elseif ( $type == 'COMPANY' )
                $category = 'Companies';
            elseif ( $type == 'DEAL' )
                $category = 'Deals';
            else continue;

            if ( $result[ $type ][ 'name' ] != $category )
                $result[ $type ][ 'name' ] = $category;

            $result[ $type ]['results'] = self::getItemData($v, $type);
        }

        $result['action'] = [
            'url' => 'https://mail.ru',
            'text' => ' All Results'
        ];

        return $result;
    }

    /**
     * @param array $data
     * @param bool $type
     * @return array
     * @throws \Bitrix\Main\ArgumentNullException
     * @throws \Bitrix\Main\ArgumentOutOfRangeException
     */
    function getItemData($data = [], $type = false )
    {
        $result = [];

        $contactOnly = Option::get( self::module, self::prefix . "with_contact_details_only") ? true : false;

        foreach ( $data as $item )
        {
            if ( $contactOnly && !$item['SEARCHED'] ) continue;

            $result[] = [
                'id'    => $item['ID'],
                'type'  => $type,
                'title' => $item['FULL_NAME'],
                //'url'   => '/crm/'. strtolower($type) .'/details/'.$item['ID'].'/',
                'description' => self::getSearchedData($item),
                //'price' => '/crm/'. strtolower($type) .'/details/'.$item['ID'].'/',

                //'extra' => '<button class="entityOpen icon mini ui button" onclick=BX.Crm.Page.openSlider("'.'/crm/'. strtolower($type) .'/details/'.$item['ID'].'/");return false;><i class="folder open outline icon"></i></button>',
                'extra' => '<button class="entityOpen icon mini ui button" href="/crm/'. strtolower($type) .'/details/'.$item['ID'].'/" ><i class="folder open outline icon"></i></button>',

                'data'  => self::updatePhones($item)
            ];
        }

        return $result;
    }

    /**
     * @param array $item
     * @return array
     */
    function updatePhones(array $item = [] )
    {
        $return = [];

        foreach ( $item['SEARCHED'] as $key => $v )
        {
            if($v['TYPE_ID'] == 'PHONE')
                $item['SEARCHED'][$key]['VALUE'] = self::phoneFormatEx($v['VALUE']);
        }

        return $item;
    }

    /**
     * @param array $item
     * @return string
     */
    function getSearchedData(array $item = [] )
    {
        $return = [];

        foreach ( $item['SEARCHED'] as $v )
        {
            if($v['TYPE_ID'] == 'PHONE')
                $v['VALUE'] = self::phoneFormatEx($v['VALUE']);

            $return[] = $v['VALUE'] ;
        }

        return implode(',<br>', $return);
    }

    /**
     * @return array
     */
    function entityTypes()
    {
        return array (
            /*'GROUPS' =>
                array (
                    'options' =>
                        array (
                            'context' => 'SEARCH',
                            'enableAll' => 'N',
                            'enableEmpty' => 'N',
                            'enableUserManager' => 'N',
                        ),
                ),*/
            'CONTACTS' =>
                array (
                    'options' =>
                        array (
                            'enableSearch' => 'Y',
                            'searchById' => 'Y',
                            'addTab' => 'Y',
                            'onlyWithEmail' => 'N',
                            'prefixType' => 'FULL',
                            'returnItemUrl' => 'Y'
                        ),
                ),
            'COMPANIES' =>
                array (
                    'options' =>
                        array (
                            'enableSearch' => 'Y',
                            'searchById' => 'Y',
                            'addTab' => 'Y',
                            'onlyWithEmail' => 'N',
                            'onlyMy' => 'N',
                            'prefixType' => 'FULL',
                            'returnItemUrl' => 'Y',
                        ),
                ),
            'LEADS' =>
                array (
                    'options' =>
                        array (
                            'enableSearch' => 'Y',
                            'searchById' => 'Y',
                            'addTab' => 'Y',
                            'onlyWithEmail' => 'N',
                            'prefixType' => 'FULL',
                            'returnItemUrl' => 'Y',
                        ),
                ),
            'DEALS' =>
                array (
                    'options' =>
                        array (
                            'enableSearch' => 'Y',
                            'searchById' => 'Y',
                            'addTab' => 'Y',
                            'prefixType' => 'FULL',
                            'returnItemUrl' => 'Y',
                        ),
                ),
            /*'PRODUCTS' =>
                array (
                    'options' =>
                        array (
                            'enableSearch' => 'Y',
                            'searchById' => 'Y',
                            'addTab' => 'Y',
                            'prefixType' => 'FULL',
                            'returnItemUrl' => 'Y',
                        ),
                )*/
        );
    }

    /**
     * @param $phone
     * @return string
     */
    public static function phoneFormatEx($phone)
    {
        $onlyNumber = preg_replace("/[^\d]/", '', $phone);

        if ( strlen($onlyNumber) == 10 && $onlyNumber[0] != '1' )
            $phone = '+1' . $phone;
        if ( strlen($onlyNumber) > 10 && $onlyNumber[0] != '+')
            $phone = '+' . $phone;

        $res = \Bitrix\Main\PhoneNumber\Parser::getInstance()->parse( $phone );

        if(!$res->isValid()) return $phone;

        $string = '+' . self::phoneFormat($res->format(\Bitrix\Main\PhoneNumber\Format::INTERNATIONAL));

        $string.= ' [';
            $string.=     $res->getCountry();
            $string.=   ', ';
            $string.=     $res->getNumberType();
        $string.= ']';

        return $string;

        //$res->getRawNumber();
        //$res->format(\Bitrix\Main\PhoneNumber\Format::E164);
        //$res->format(\Bitrix\Main\PhoneNumber\Format::NATIONAL);
    }

    /**
     * @param string $phone
     * @param bool $convert
     * @param bool $trim
     * @return false|string|string[]|null
     */
    static function phoneFormat($phone = '', $convert = false, $trim = true)
    {
        if (empty($phone))
            return '';
        $phone = preg_replace("/[^0-9A-Za-z]/", "", $phone);
        if ($convert == true)
        {
            $replace = array('2'=>array('a','b','c'),
                '3'=>array('d','e','f'),
                '4'=>array('g','h','i'),
                '5'=>array('j','k','l'),
                '6'=>array('m','n','o'),
                '7'=>array('p','q','r','s'),
                '8'=>array('t','u','v'), '9'=>array('w','x','y','z'));
            foreach($replace as $digit=>$letters)
                $phone = str_ireplace($letters, $digit, $phone);
        }

        if ($trim == true && strlen($phone)>11)
            $phone = substr($phone,  0, 11);
        if (strlen($phone) == 7)
            return preg_replace("/([0-9a-zA-Z]{3})([0-9a-zA-Z]{4})/", "$1-$2", $phone);
        elseif (strlen($phone) == 10)
            return preg_replace("/([0-9a-zA-Z]{3})([0-9a-zA-Z]{3})([0-9a-zA-Z]{2})([0-9a-zA-Z]{2})/", "$1-$2-$3$4", $phone);
        elseif (strlen($phone) == 11)
            return preg_replace("/([+0-9a-zA-Z]{1})([0-9a-zA-Z]{3})([0-9a-zA-Z]{3})([0-9a-zA-Z]{2})([0-9a-zA-Z]{2})/", "$1 ($2)$3-$4$5", $phone);

        return $phone;
    }
}