<?php
//v2
if(!$USER->IsAdmin())
    die( 'Only Admins' );

//\Bitrix\Main\Loader::requireModule("calendar");

use \Bitrix\Main\Config\Option;

$groupArray[] = '...';
$db = \CGroup::GetList(
    $by = "name",
    $order = "asc",
    ['ACTIVE' => 'Y'],
    "Y"
);
while( $list = $db->Fetch() )
    $groupArray[ $list['ID'] ] = $list['REFERENCE'];

/*$db = \Bitrix\Calendar\Internals\SectionTable::getList(
  [
      'select'  => ['ID', 'NAME'],
      'filter'   => [
        'CAL_TYPE'  => 'company_calendar'
      ]
  ]
);
while ($list = $db->Fetch())
    $arCalendar[ $list['ID'] ] = $list['NAME'];

$db = \Bitrix\Iblock\IblockTable::getList([
    'select' => ['ID', 'NAME'],
    'limit' => 200
]);
while ($list = $db->Fetch())
    $arIblocks[ $list['ID'] ] = $list['NAME'];

$db = \Bitrix\Highloadblock\HighloadBlockTable::getList([
    'select' =>  ['*'],
    'limit'  =>  100
]);

while ( $list = $db->fetch() )
    $highloadBlocks[ $list['ID'] ] = $list['TABLE_NAME'];

$propId = \Bitrix\Iblock\PropertyTable::getList(array(

    'select' => ['ID'],
    'filter' => [
        'IBLOCK_ID' =>  Option::get( $module_id, 'AHC_ABSENT_IBLOCK_ID' ) ? : 3,
        'CODE'      =>  'ABSENCE_TYPE'
    ]

))->fetch()['ID'];

$dbEnums = \Bitrix\Iblock\PropertyEnumerationTable::getList(array(
    'order'     => array('VALUE' => 'asc'),
    'select'    => array('ID', 'VALUE'),
    'filter'    => array('PROPERTY_ID' => $propId)
));
while($arEnum = $dbEnums->fetch())
    $arAbsenceTypes[$arEnum['ID']] = $arEnum['VALUE'];
*/

global $USER_FIELD_MANAGER;
foreach( $USER_FIELD_MANAGER->GetUserFields('CRM_LEAD', false, 'en') as $propId => $array )
    $userFields['CRM_LEAD'][$propId] = $array['EDIT_FORM_LABEL'];
$providerArray[] = ['no class name'];

//foreach( \Bitrix\Crm\Activity\Provider\ProviderManager::getCompletableProviderList() as $id => $value )
//    $providerArray[ $value ['ID'] ] = $value['NAME'] . ' | ' . $value ['ID'];
$provArray = \Bitrix\Crm\Activity\Provider\ProviderManager::getProviders();
foreach ( $provArray as $key => $value )
    $provArray[$key] = $value::getName();
$provArray['ANY'] = 'Any Activity';

$prefix = 'ahc_card_';
$aTabs =  array(

    array(

        "DIV"       => "main",
        "TAB"       => 'Main',
        "TITLE"     => 'Preferences',

        "OPTIONS" =>    [

            'Main Setup',

            /*[
                $prefix . "enable",
                "Enhance Call Card Enable",
                Option::get($module_id, $prefix . "enable"),
                array("checkbox")
            ],

            [
                $prefix . "simple_mode_off",
                "Simple Card Off Mode",
                Option::get($module_id, $prefix . "simple_mode_off"),
                array("checkbox")
            ],*/

            [
                $prefix . "prop_lead_type",
                "Property Lead Type",
                Option::get($module_id, $prefix . "prop_lead_type") ? : 'UF_CRM_1546442504',
                array("selectbox", $userFields['CRM_LEAD'])
            ],

            [
                $prefix . "prop_market_type",
                "Property Market Type",
                Option::get($module_id, $prefix . "prop_market_type") ? : 'UF_CRM_1545325186',
                array("selectbox", $userFields['CRM_LEAD'])
            ],

            [
                $prefix . "count_event",
                "Number of last events",
                Option::get($module_id, $prefix . "count_event") ? : 3,
                array("text", 3 )
            ],

            [
                $prefix . "allowed_event_types",
                "Allowed Event Types",
                Option::get($module_id, $prefix . "allowed_event_types"),
                array("multiselectbox", $provArray )
            ],

            [
                $prefix . "groups_resposible",
                "Responsible Users Groups",
                Option::get($module_id, $prefix . "groups_resposible"),
                array("multiselectbox", $groupArray )
            ],

            [
                $prefix . "with_contact_details_only",
                "Search results with data contacts only",
                Option::get($module_id, $prefix . "with_contact_details_only"),
                array("checkbox")
            ],

            [
                $prefix . "cache",
                "Cache (ttl)",
                Option::get($module_id, $prefix . "cache") ? : 3600,
                array("text", 6 )
            ],

            [
            $prefix . "debug",
                "Debug Mode",
                Option::get($module_id, $prefix . "debug"),
                array("checkbox")
            ],

            '<a target="_blank" href="/local/modules/'.$module_id.'/test.php">testing page</a>'
        ]
    ),

);