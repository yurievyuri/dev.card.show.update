<?php
global $DBType;
\Bitrix\Main\Loader::registerAutoLoadClasses('dev.card.show',
    [
        '\Dev\Call\Card'         =>     'classes/Card.php',
        '\Dev\Call\Functions'    =>     'classes/Functions.php',
        '\Dev\Call\AppPlacements'   =>     'classes/AppPlacements.php'
    ]
);
//require __DIR__ . '/css/include.php';
//require __DIR__ . '/js/include.php';